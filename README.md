ansible-role-packages
=========

This role installs distro, flatpak and pip packages depending on defined variables

Role Variables
--------------

Well-defined variables - for reference please check out defaults/main.yml and the variables [here](https://gitlab.com/ansible-opletal/ansible-workstation/ansible-linux-workstation/-/tree/master/vars)

Dependencies
------------

None

License
-------

BSD
